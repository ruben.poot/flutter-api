import 'package:flutter/cupertino.dart';
import 'package:flutter_api_rest/src/models/news_models.dart';


import 'package:http/http.dart' as http;

const _url = 'https://newsapi.org/v2';
const _apikey='279952d13f4b40c7aaf8d5e6170d6a88';

class NewsService with ChangeNotifier{
  List<Article> noticias = [];

  NewsService(){
    getNoticias();
  }

  getNoticias() async{
    final  url = Uri.parse("$_url/top-headlines?country=us&category=business&apiKey=$_apikey");
    final resp = await http.get(url);
    final newsResponse = newsResponseFromJson(resp.body);
    noticias.addAll(newsResponse.articles);
    notifyListeners();
  }
}