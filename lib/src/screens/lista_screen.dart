import 'package:flutter/material.dart';
import 'package:flutter_api_rest/src/services/news_service.dart';
import 'package:provider/provider.dart';

class ListaScreen extends StatelessWidget {
   
  const ListaScreen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {

    final noticias = Provider.of<NewsService>(context).noticias;

    return Scaffold(
      body: ListView.builder(
        itemCount: noticias.length,
        itemBuilder: (BuildContext context,int index){
          return Text(noticias[index].title);
        }),
    );
  }
}