import 'package:flutter/material.dart';
import 'package:flutter_api_rest/src/screens/lista_screen.dart';
import 'package:flutter_api_rest/src/services/news_service.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => NewsService(),
        ),
      ],
      child: MaterialApp(
        title: 'Material App',
        home: Scaffold(
          appBar: AppBar(
            title: Text('Material App Bar'),
          ),
          body: ListaScreen(),
        ),
      ),
    );
  }
}
